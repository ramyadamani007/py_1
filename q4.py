a = 9
b = 65
print("Bitwise AND Operator On 9 and 65 is = ", a & b)
print("Bitwise OR Operator On 9 and 65 is = ", a | b)
print("Bitwise EXCLUSIVE OR Operator On 9 and 65 is = ", a ^ b)
print("Bitwise NOT Operator On 9 is = ", ~a)
print("Bitwise LEFT SHIFT Operator On 9 is = ", a << 1)
print("Bitwise RIGHT SHIFT Operator On 65 is = ", b >> 1)

#output
#('Bitwise AND Operator On 9 and 65 is = ', 1)
#('Bitwise OR Operator On 9 and 65 is = ', 73)
#('Bitwise EXCLUSIVE OR Operator On 9 and 65 is = ', 72)
#('Bitwise NOT Operator On 9 is = ', -10)
#('Bitwise LEFT SHIFT Operator On 9 is = ', 18)
#('Bitwise RIGHT SHIFT Operator On 65 is = ', 32)

