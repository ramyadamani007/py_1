x = 15
y = 4
print('x + y =', x + y)
print('x - y =', x - y)
print('x * y =', x * y)
print('x / y =', x / y)
print('x // y =', x // y)
print('x ** y =', x ** y)
print('x % y =', x % y)

#output
#('x + y =', 19)
#('x - y =', 11)
#('x * y =', 60)
#('x / y =', 3)
#('x // y =', 3)
#('x ** y =', 50625)
#('x % y =', 3)

